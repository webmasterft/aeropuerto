markers = [
    {
        "number": 1,
        "color": "DarkGreen",
        "es": "De Volada",
        "en": "To Go",
        "logo": "ab10",
        "lat": 519.25,
        "lng": 991.5
    },
    {
        "number": 2,
        "color": "DarkGreen",
        "es": "Amazonia Cafe",
        "en": "",
        "logo": "ab02",
        "lat": 479.5,
        "lng": 1180.25
    },
    {
        "number": 3,
        "color": "DarkGreen",
        "es": "Tres Trio",
        "en": "",
        "logo": "ab03",
        "lat": 223.25,
        "lng":333
    },
    {
        "number": 1,
        "color": "Orange",
        "es": "Flor de Liz",
        "en": "",
        "logo": "t01",
        "lat": 458,
        "lng": 1138.75
    },
    {
        "number": 2,
        "color": "Orange",
        "es": "Wifitogo",
        "en": "",
        "logo": "t02",
        "lat": 319,
        "lng": 1061.75
    },
    {
        "number": 3,
        "color": "Orange",
        "es": "Attenza Duty Free",
        "en": "",
        "logo": "t03",
        "lat": 272.5,
        "lng": 1056
    },
    {
        "number": 1,
        "color": "Green",
        "es": "Servicio de taxi",
        "en": "Cab service",
        "logo": "s01",
        "lat": 519,
        "lng": 1029.25
    },
    {
        "number": 2,
        "color": "Green",
        "es": "Urbapark",
        "en": "",
        "logo": "s13",
        "lat": 516.75,
        "lng": 1066.25
    },
    {
        "number": 3,
        "color": "Green",
        "es": "Quito Turismo",
        "en": "",
        "logo": "s14",
        "lat": 508,
        "lng": 1064.5
    },
    {
        "number": 4,
        "color": "Green",
        "es": "Renta de autos",
        "en": "Car rental",
        "logo": "000",
        "lat": 489.75,
        "lng": 892.75
    },
    {
        "number": 5,
        "color": "Green",
        "es": "Aeroservicios",
        "en": "",
        "logo": "s15",
        "lat": 515.25,
        "lng": 1148.5
    },
    {
        "number": 6,
        "color": "Green",
        "es": "Carsa Telefonía e Internet",
        "en": "",
        "logo": "000",
        "lat": 453.25,
        "lng": 936.25
    },
    {
        "number": 7,
        "color": "Green",
        "es": "Servicio de Taxi",
        "en": "Cab service",
        "logo": "s01",
        "lat": 437.5,
        "lng": 948
    },
    {
        "number": 8,
        "color": "Green",
        "es": "Global Exchange",
        "en": "",
        "logo": "s03",
        "lat": 463,
        "lng": 1096.25
    },
    {
        "number": 9,
        "color": "Green",
        "es": "Global Exchange",
        "en": "",
        "logo": "s03",
        "lat": 435.5,
        "lng": 1094.25
    },
    {
        "number": 10,
        "color": "Green",
        "es": "Bag Parking",
        "en": "",
        "logo": "s10",
        "lat": 457.75,
        "lng": 1154.75
    },
    {
        "number": 11,
        "color": "Green",
        "es": "Carritos para equipaje",
        "en": "Luggage Carts",
        "logo": "",
        "lat": 326.25,
        "lng": 1047.25
    }    
];
markers2 = [
    {
        "number": 1,
        "color": "DarkGreen",
        "es": "Amazonia Cafe",
        "en": "",
        "logo": "ab02",
        "lat": 518.5,
        "lng": 1035
    },
    {
        "number": 2,
        "color": "DarkGreen",
        "es": "Johnny Rockets",
        "en": "",
        "logo": "ab04",
        "lat": 480,
        "lng": 1181
    },
    {
        "number": 3,
        "color": "DarkGreen",
        "es": "De Volada",
        "en": "On the fly",
        "logo": "ab01",        
        "lat": 216,
        "lng": 1311.5
    },
    {
        "number": 4,
        "color": "DarkGreen",
        "es": "OutBack",
        "en": "",
        "logo": "ab06",
        "lat": 214.69998931884766,
        "lng": 1249
    },
    {
        "number": 5,
        "color": "DarkGreen",
        "es": "Johnny Rockets",
        "en": "",
        "logo": "ab04",
        "lat": 225,
        "lng": 1098.5
    },
    {
        "number": 6,
        "color": "DarkGreen",
        "es": "Fly Chicken Fly",
        "en": "",
        "logo": "ab07",
        "lat": 216.3499937057495,
        "lng": 1063.5500030517578
    },
    {
        "number": 7,
        "color": "DarkGreen",
        "es": "Amazonia Cafe",
        "en": "",
        "logo": "ab02",
        "lat": 216.24999713897705,
        "lng": 1049.5
    },
    {
        "number": 8,
        "color": "DarkGreen",
        "es": "Famous Famiglia",
        "en": "",
        "logo": "ab08",
        "lat": 220.99999713897705,
        "lng": 1012
    },
    {
        "number": 9,
        "color": "DarkGreen",
        "es": "De Volada",
        "en": "On the fly",
        "logo": "ab01",
        "lat": 215.3499994277954,
        "lng": 744.6999893188477
    },
    
    {
        "number": 10,
        "color": "DarkGreen",
        "es": "Guacamole Grill",
        "en": "",
        "logo": "ab09",
        "lat": 207.74999713897705,
        "lng": 503.5
    },
    {
        "number": 11,
        "color": "DarkGreen",
        "es": "Amazonia Cafe",
        "en": "",
        "logo": "ab02",
        "lat": 229.39999866485596,
        "lng": 400.9499969482422
    },
    {
        "number": 1,
        "color": "Orange",
        "es": "Ecuador Travel Stores",
        "en": "",
        "logo": "t01",
        "lat": 480,
        "lng": 1101.5
    },
    {
        "number": 2,
        "color": "Orange",
        "es": "Plaza Ruana",
        "en": "",
        "logo": "t02",
        "lat": 460.5,
        "lng": 1028.75
    },
    {
        "number": 3,
        "color": "Orange",
        "es": "Attenza Duty Free",
        "en": "",
        "logo": "t03",
        "lat": 284.75,
        "lng": 1228.25
    },
    {
        "number": 4,
        "color": "Orange",
        "es": "Wifitogo",
        "en": "",
        "logo": "t04",
        "lat": 251.5999984741211,
        "lng": 1175.5500049591064
    },

    {
        "number": 5,
        "color": "Orange",
        "es": "Toc Gallery",
        "en": "",
        "logo": "t05",
        "lat": 238.0999984741211,
        "lng": 1188.5500049591064
    },
    {
        "number": 6,
        "color": "Orange",
        "es": "Bijoux Terner",
        "en": "",
        "logo": "t06",
        "lat": 238.5,
        "lng": 1158.6999988555908
    },
    {
        "number": 7,
        "color": "Orange",
        "es": "Ecuador a Bordo",
        "en": "",
        "logo": "t07",
        "lat": 251.75,
        "lng": 1158.9499988555908
    },
    {
        "number": 8,
        "color": "Orange",
        "es": "Attenza Tech",
        "en": "",
        "logo": "t03",
        "lat": 259.75,
        "lng": 1129.4499988555908
    },
    {
        "number": 9,
        "color": "Orange",
        "es": "The Legendary Hat Company",
        "en": "",
        "logo" : "t09",
        "lat": 221.5,
        "lng": 1117.25
    },
    {
        "number": 10,
        "color": "Orange",
        "es": "República del Cacao",
        "en": "",
        "logo" : "t10",
        "lat": 264,
        "lng": 1099.25
    },
    {
        "number": 11,
        "color": "Orange",
        "es": "Mac",
        "en": "",
        "logo" : "t11",
        "lat": 263.5,
        "lng": 1071.25
    },
    {
        "number": 12,
        "color": "Orange",
        "es": "Galerías Tikuna",
        "en": "",
        "logo" : "t12",
        "lat": 267.25,
        "lng": 1015.25
    },
    {
        "number": 13,
        "color": "Orange",
        "es": "Samsung",
        "en": "",
        "logo" : "t13",
        "lat": 220.75,
        "lng": 993.75
    },
    {
        "number": 14,
        "color": "Orange",
        "es": "Ecuador Travel Stores",
        "en": "",
        "logo" : "t01",
        "lat": 257.5,
        "lng": 973.25
    },
    {
        "number": 15,
        "color": "Orange",
        "es": "Mayta & Co.",
        "en": "",
        "logo" : "t14",
        "lat": 237.75,
        "lng": 962.25
    },
    {
        "number": 16,
        "color": "Orange",
        "es": "República del Cacao",
        "en": "",
        "logo" : "t10",
        "lat": 238,
        "lng": 936
    },
    {
        "number": 17,
        "color": "Orange",
        "es": "Adidas",
        "en": "",
        "logo" : "t15",
        "lat": 267,
        "lng": 934.25
    },
    {
        "number": 18,
        "color": "Orange",
        "es": "Last Chance",
        "en": "",
        "logo": "",
        "lat": 262.5,
        "lng": 905.25
    },
    {
        "number": 19,
        "color": "Orange",
        "es": "Smart Phone Solutions",
        "en": "",
        "logo": "t17",
        "lat": 238,
        "lng": 917
    },
    {
        "number": 20,
        "color": "Orange",
        "es": "Quri",
        "en": "",
        "logo": "t18",
        "lat": 235,
        "lng": 835
    },
    {
        "number": 21,
        "color": "Orange",
        "es": "Fly Massage",
        "en": "",
        "logo": "t19",
        "lat": 272.25,
        "lng": 883
    },
    {
        "number": 22,
        "color": "Orange",
        "es": "Uomo Venetto",
        "en": "",
        "logo": "t20",
        "lat": 297.25,
        "lng": 848.25
    },
    {
        "number": 23,
        "color": "Orange",
        "es": "Ecuador Travel Stores",
        "en": "",
        "logo": "t01",
        "lat": 265.25,
        "lng": 765.75
    },

    {
        "number": 1,
        "color": "Green",
        "es": "Servicios Don Julio",
        "en": "Shoeshining Service",
        "logo": "s12",
        "lat": 453.5,
        "lng": 938.5
    },
    {
        "number": 2,
        "color": "Green",
        "es": "Secure Wrap",
        "en": "",
        "logo": "s02",
        "lat": 457.25,
        "lng": 999.75
    },
    {
        "number": 3,
        "color": "Green",
        "es": "Secure Wrap",
        "en": "",
        "logo": "s02",
        "lat": 457,
        "lng": 1061.75
    },
    {
        "number": 4,
        "color": "Green",
        "es": "Global Exchange",
        "en": "",
        "logo": "s03",
        "lat": 519,
        "lng": 1104.9
    },
    {
        "number": 5,
        "color": "Green",
        "es": "Secure Wrap",
        "en": "",
        "logo": "s02",
        "lat": 456.55,
        "lng": 1107.5
    },
    {
        "number": 6,
        "color": "Green",
        "es": "Assist Card",
        "en": "",
        "logo": "s04",
        "lat": 457.15001678466797,
        "lng": 1129.5
    },
    {
        "number": 7,
        "color": "Green",
        "es": "Carritos para equipaje",
        "en": "Luggage Carts",
        "logo": "s05",
        "lat": 537,
        "lng": 966.5
    },
    {
        "number": 8,
        "color": "Green",
        "es": "Carritos para equipaje",
        "en": "Luggage Carts",
        "logo": "s05",
        "lat": 537,
        "lng": 1099.5
    }
];